#Noise
Only comes with perlin right now, but maybe I'll add others
Took a while to extract the idea into golang PROPERLY for any dimension
I copied Perlin's original method and started playing around with 2d and 3d and trying to make new connections
Stuck on a few typos now and then too cause that's always fun
Anyway, the two exported functions you should worry about are `Perlin` and `OctavePerlin`. `Perlin` takes any number of float64s (would not recommend high dimensional tuple because perlin pretty bad at that). `OctavePerlin` pretty much adds noise to itself with different amplitudes so you get a nicer looking noise
Tests are pretty shitty because I always have issue with testing stuff like pseudorng things because you would have to go through the algo by hand to make sure you're right. Tests have an image writing method so you can see the result and determine if it looks noisy enough
