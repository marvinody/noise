package noise

import (
	"math"
)

// "1d" array of tuples
// look at it at an array of []float64s where the float64s are accessed with 1 idx
const gradLength int = 256

var perm [256]int
var p [512]int

func init() {
	perm = [256]int{151, 160, 137, 91, 90, 15,
		131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
		190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
		88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
		77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
		102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
		135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
		5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
		223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
		129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
		251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
		49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
		138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180}
	for i := 0; i < 256; i++ {
		p[i] = perm[i]
		p[256+i] = perm[i]
	}
}

/*
	I wrote this while brainstorming the first few versions of n-dim perlin
	and idk the name of the algo. It's not exactly a cartesian product
	but it takes 2 things of equal length and you get 2^len out
	cart. product is len^2
	if you pass {0, 1}, {2, 3}
	you should get
	02
	03
	12
	13 which looks cartesian
	but 3 {0,1,2}, {3,4,5}
	[0 1 2] [3 1 2] [0 4 2] [3 4 2] [0 1 5] [3 1 5] [0 4 5] [3 4 5]]
*/
func idkname(A, B []int) [][]int {
	dim := len(A)
	length := int(math.Pow(2, float64(dim)))
	set := make([][]int, length)
	for idx := range set {
		tuple := make([]int, dim)
		for d := 0; d < dim; d += 1 {
			if (idx & (1 << uint(d))) > d {
				tuple[d] = B[d]
			} else {
				tuple[d] = A[d]
			}
		}
		set[idx] = tuple
	}

	return set
}

// Octave Perlin calls Perlin repeatedly octaves times
// https://flafla2.github.io/2014/08/09/perlinnoise.html explains it better
func OctavePerlin(x, y, z float64, octaves int, persistence float64) float64 {
	var total, frequency, amplitude, maxValue float64 = 0, 1, 1, 0
	for i := 0; i < octaves; i++ {
		total += Perlin(x*frequency, y*frequency, z*frequency) * amplitude
		maxValue += amplitude
		amplitude *= persistence
		frequency *= 2
	}
	return total / maxValue

}

// combines a bool slice into an int (MSB on left)
// works with breakIntoBits to generate sequence A030109
func combineIntoInt(bools []bool) int {
	total := 0
	for idx, b := range bools {
		if b {
			total += 1 << uint(len(bools)-1-idx)
		}
	}
	return total
}

// goes from 0 -> n-1, breaking each number into bool slices with MSB on right (normally it's left, important!)
func breakIntoBits(n, bits int) [][]bool {
	bools := make([][]bool, n)
	for idx := 0; idx < n; idx += 1 {
		bools[idx] = make([]bool, bits) // align cap to nearest multiple of 8?
		for i := 0; i < bits; i += 1 {
			b := (1<<uint(i))&idx > 0
			bools[idx][i] = b
		}
	}
	return bools

}
func createSubcorner(root, extra int) (int, int) {
	return p[root+0] + extra, p[root+1] + extra
}
func createCornerHashes(bounds []int) []int {
	A, B := createSubcorner(bounds[0], bounds[1])
	corners := make([]int, 0, 2)
	corners = append(corners, A, B)
	for i := 1; i < len(bounds); i += 1 {
		temp := make([]int, len(corners)*2)
		extra := 0
		if i+1 < len(bounds) {
			extra = bounds[i+1]
		}
		for idx, corner := range corners {
			cornerA, cornerB := createSubcorner(corner, extra)
			temp[2*idx+0] = cornerA
			temp[2*idx+1] = cornerB
		}
		corners = temp
	}
	return corners
}

func createGradientFunc(dim int) func(int, ...float64) float64 {
	grads := make([][]int, 0, dim*2*(dim-1)) // choose from n dimensions, no repeat, 2 ops
	bools := breakIntoBits((dim-1)*2, dim)
	for d := dim - 1; d > -1; d -= 1 {
		for tupleIdx := 0; tupleIdx < (dim-1)*2; tupleIdx += 1 {
			grad := make([]int, dim)
			for i := range grad {
				moddedI := i // this is because we skip a "bit"
				if i > d {   // but the bools method doesn't know that
					moddedI -= 1 // so we sub 1 if we're over the skipped dim
				}
				switch {
				case i == d:
					grad[i] = 0 // ignored dim
				case bools[tupleIdx][moddedI]: // "go up" by binary and set to dec
					grad[i] = -1
				default: // start as 1
					grad[i] = 1
				}
			}
			grads = append(grads, grad)
		}
	}
	return func(hash int, tuple ...float64) float64 {
		grad := grads[hash%len(grads)]

		total := 0.0
		for d := range tuple {
			total += float64(grad[d]) * tuple[d]
		}
		return total
	}
}

// Perlin returns a float with the basic perlin noise idea
// tuples with only integers should only return 0
// so scale your stuff if you're working with pixels
func Perlin(tuple ...float64) float64 {
	lowerBound := make([]int, len(tuple))
	relative := make([]float64, len(tuple))
	fades := make([]float64, len(tuple))
	for d := range tuple {
		lowerBound[d] = int(math.Floor(tuple[d])) & 255
		relative[d] = tuple[d] - math.Floor(tuple[d])
		tuple[d] -= tuple[d]
		fades[d] = fade(relative[d])
	}

	hashes := createCornerHashes(lowerBound)
	gradFunc := createGradientFunc(len(tuple))
	values := make([]float64, len(hashes))
	// make 2^dim entries and break into dim bits
	bools := breakIntoBits(len(hashes), len(tuple))
	// populate values with initial stuff
	for idx := range values {
		moddedTuple := make([]float64, len(tuple))
		for d := range moddedTuple {
			if bools[idx][d] {
				moddedTuple[d] = relative[d] - 1
			} else {
				moddedTuple[d] = relative[d]
			}
		}
		correctIdx := combineIntoInt(bools[idx])
		values[idx] = gradFunc(hashes[correctIdx], moddedTuple...)
	}
	// lerp stuff together now
	for d := range tuple {
		tempVals := make([]float64, len(values)/2)
		t := fades[d]
		for idx := range tempVals {
			tempVals[idx] = lerp(t, values[2*idx+0], values[2*idx+1])
		}
		values = tempVals
	}
	// pass 2^n, n where n := len(tuple)
	return values[0]

}

func fade(t float64) float64 {
	return t * t * t * (t*(t*6-15) + 10)
}

func lerp(t, a, b float64) float64 {
	return a + t*(b-a)
}
