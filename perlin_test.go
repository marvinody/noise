package noise

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
	"testing"
)

func TestPerlinImage(t *testing.T) {
	size := 256
	scale := 2 / float64(size)
	low, high := 3.0, -1.0
	img := image.NewGray(image.Rect(0, 0, size, size))
	for y := 0; y < size; y += 1 {
		for x := 0; x < size; x += 1 {
			scaledX, scaledY := float64(x)*scale, float64(y)*scale
			f := Perlin(scaledX, scaledY)
			f = (f + 1) / 2
			clr := uint8(255 * f)
			if f < low {
				low = f
			}
			if f > high {
				high = f
			}
			img.Set(x, y, color.Gray{clr})
		}
	}
	fmt.Println("low:", low)
	fmt.Println("high:", high)
	file, _ := os.Create("perlin.png")
	png.Encode(file, img)

}

func TestOctaveImage(t *testing.T) {
	size := 256
	scale := 0.5 / float64(size)
	low, high := 3.0, -1.0
	img := image.NewGray(image.Rect(0, 0, size, size))
	for y := 0; y < size; y += 1 {
		for x := 0; x < size; x += 1 {
			scaledX, scaledY := float64(x)*scale, float64(y)*scale
			f := OctavePerlin(scaledX, scaledY, 1, 5, 3)
			clr := uint8(255 * f)
			f = (f + 1) / 2
			if f < low {
				low = f
			}
			if f > high {
				high = f
			}
			img.Set(x, y, color.Gray{clr})
		}
	}
	fmt.Println("low:", low)
	fmt.Println("high:", high)
	file, _ := os.Create("perlin_octave.png")
	png.Encode(file, img)
}
